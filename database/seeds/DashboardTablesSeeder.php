<?php

use App\Permission;

class DashboardTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate(['dashboard_sections', 'dashboard_submenus', 'dashboard_links']);

        $permissions = Permission::all('id', 'key_name')->pluck('id', 'key_name');

        foreach ($this->getData() as $i => $section) {
            $sectionId = DB::table('dashboard_sections')->insertGetId([
                'name'       => $section['name'],
                'tile'       => $section['tile'],
                'order'      => $i + 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            foreach ($section['submenus'] as $j => $submenu) {
                $submenuId = DB::table('dashboard_submenus')->insertGetId([
                    'name'       => $submenu['name'],
                    'icon'       => $submenu['icon'],
                    'order'      => $j + 1,
                    'section_id' => $sectionId,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                foreach ($submenu['links'] as $k => $link) {
                    DB::table('dashboard_links')->insert([
                        'name'          => $link['name'],
                        'route'         => $link['route'],
                        'order'         => $k + 1,
                        'submenu_id'    => $submenuId,
                        'permission_id' => $permissions[$link['permission']],
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s')
                    ]);
                }
            }
        }
    }



    /**
     * Return the data to populate tables.
     *
     * @return array
     */
    private function getData()
    {
        return [

            [
                'name' => 'Organigrama',
                'tile' => 'users.png',
                'submenus' => [
                    [
                        'name' => 'Organigrama',
                        'icon' => 'users.png',
                        'links' => [
                            [
                                'name'       => 'Agregar Organigrama',
                                'route'      => 'organigrama/agregar',
                                'permission' => 'create.users'
                            ],
                            [
                                'name'       => 'Ver Organigramas',
                                'route'      => 'organigrama/tabla',
                                'permission' => 'view.users'
                            ],
                        ]
                    ],

                ]
            ],
            [
                'name' => 'Perfil',
                'tile' => 'users.png',
                'submenus' => [
                    [
                        'name' => 'Perfil',
                        'icon' => 'users.png',
                        'links' => [
                            [
                                'name'       => 'Agregar Perfil',
                                'route'      => 'perfiles/agregar',
                                'permission' => 'create.users'
                            ],
                            [
                                'name'       => 'Ver Perfiles',
                                'route'      => 'perfiles/consultar',
                                'permission' => 'view.users'
                            ],
                        ]
                    ],

                ]
            ],
            [
                'name' => 'Usuarios',
                'tile' => 'users.png',
                'submenus' => [
                    [
                        'name' => 'Usuario',
                        'icon' => 'users.png',
                        'links' => [
                            [
                                'name'       => 'Agregar Usuario',
                                'route'      => 'users/agregar',
                                'permission' => 'create.users'
                            ],
                            [
                                'name'       => 'Visualizar Usuario',
                                'route'      => 'users/tabla',
                                'permission' => 'view.users'
                            ],
                        ]
                    ],

                ]
            ],

        ];
    }
}
