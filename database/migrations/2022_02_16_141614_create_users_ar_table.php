<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersARTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_ar', function (Blueprint $table) {
            $table->id();
            $table->string('key_code');
            $table->string('last_name');
            $table->string('name');
            $table->unsignedInteger('phone');
            $table->string('profile');
            $table->unsignedInteger('failed_attempts');
            $table->string('account_status');
            $table->string('email');
            $table->timestamps();

            //$table->foreign('profile')->references('name')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_ar');
    }
}
