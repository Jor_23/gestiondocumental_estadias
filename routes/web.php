<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');


Route::get('/ejemplo', 'EjemploController@index');
Route::post('/guardar-rol', 'EjemploController@save');
Route::get('/editar-rol/{key_name}', 'EjemploController@edit');
Route::put('/actualizar-rol/{key_name}', 'EjemploController@update');
Route::get('/eliminar-rol/{key_name}', 'EjemploController@delete');
//Route::view('/ejemplo', 'ejemplo');



Route::get('iniciar-sesion', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('iniciar-sesion', 'Auth\LoginController@login');
Route::view('registro', 'Auth.register');
Route::get('cerrar-sesion', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'noCache']], function() {
    Route::view('/', 'admin/dashboard');
    Route::get('/roles', 'EjemploController@index');
    Route::delete('/roles/eliminar/{id}', 'EjemploController@delete');

    //Profile
    Route::view('perfil/editar', 'admin.editar-perfil');
    Route::view('perfil/agregar', 'admin.perfil.crear');
    Route::post('perfil/editar', 'Admin\ProfileController@update');

    //Password
    Route::view('perfil/cambiar-contrasena', 'admin.cambiar-contrasena');
    Route::post('perfil/cambiar-contrasena', 'Auth\PasswordController@update');

    //perfiles
    Route::view('perfiles/agregar', 'admin.perfiles.crear');
    Route::view('perfiles/eliminar', 'admin.perfiles.eliminar');
    Route::view('perfiles/editar', 'admin.perfiles.editar');
    Route::view('perfiles/consultar', 'admin.perfiles.consultar');

    //Organigrama
    Route::view('organigrama/agregar', 'admin.organigrama.crear');
    Route::post('organigrama/insertar', 'OrganiController@save');
    Route::view('organigrama/tabla', 'admin.organigrama.tabla');
    Route::delete('organigrama/eliminar/{id}', 'OrganiController@delete');
    Route::get('organigrama/editar/{id}', 'OrganiController@edit');
    Route::put('organigrama/actualizar/{id}', 'OrganiController@update');
    Route::get('organigrama/tabla', 'OrganiController@index');

    //Usuarios
    Route::view('users/agregar', 'admin.users.crear');
    Route::post('users/crear', 'UsersController@save');
    Route::view('users/tabla', 'admin.users.tabla');
    Route::delete('users/eliminar/{id}', 'UsersController@delete');
    Route::get('users/editar/{id}', 'UsersController@edit');
    Route::put('users/actualizar/{id}', 'UsersController@update');
    Route::get('users/tabla', 'UsersController@index');

    //perfiles
    Route::view('perfiles/agregar', 'admin.perfiles.crear');
    Route::post('perfiles/insertar', 'PerfilesController@save');
    Route::delete('perfiles/eliminar/{id}', 'PerfilesController@delete');
    Route::get('perfiles/editar/{id}', 'PerfilesController@edit');
    Route::put('perfiles/actualizar/{id}', 'PerfilesController@update');
    Route::get('perfiles/consultar', 'PerfilesController@index');




    //Prueba
    Route::get('cerrar-sesion', 'Auth\LoginController@logout')->name('logout');
});
