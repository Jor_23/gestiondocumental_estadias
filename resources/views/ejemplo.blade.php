@extends('layout.dashboard-master')

{{-- Metadata --}}
@section('meta.title', 'Actualizar perfil' )
@section('meta.tab_title', 'Actualizar perfil | Panel de administración | ' . config('app.name'))
@section('css_classes', 'dashboard')
@section('has_gallery', 'true')

@section('content')

    <div class="dashboard-heading">
        <h1 class="dashboard-heading__title">
            Roles
        </h1>
    </div>


    <div class="fluid-container mb-16">
        <section class="db-panel">
            <h3 class="db-panel__title">
                Todos los roles
            </h3>
            <resource-table
                :breakpoint="800"
                :model="{{ $roles }}"
                inline-template
            >
                <table
                    class="table table-resource table--header-primary md:table--responsive table--full-width table--striped"
                    :class="{ 'table-resource--wide' : wideView }"
                    >
                    <thead>
                        <tr>
                            <th>
                                Nombre
                            </th>

                            <th>
                                Acciones
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="roleItem in resourceList" class="table-resource__row" :key="roleItem.id">
                            <td data-label="Nombre:">
                                @{{ roleItem.name }}
                            </td>
                            <td data-label="Acciones:">
                                <delete-button :url="$root.path + '/admin/roles/eliminar/' + roleItem.id"
                                    :resource-id="roleItem.id"
                                    :options="{ onDelete: onResourceDelete }"
                                >
                                    @svg('trash')
                                    Eliminar
                                </delete-button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </resource-table>

        </section>
    </div>

@endsection
