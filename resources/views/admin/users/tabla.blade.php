@extends('layout.dashboard-master')

{{-- Metadata --}}
@section('meta.title', 'Visualizar Usuario' )
@section('meta.tab_title', 'Visualizar Usuario | Panel de administración | ' . config('app.name'))
@section('css_classes', 'dashboard')
@section('has_gallery', 'true')

@section('content')

    <div class="dashboard-heading">
        <h1 class="dashboard-heading__title">
            Usuarios
        </h1>
    </div>


    <div class="fluid-container mb-16">
        <section class="db-panel">
            <h3 class="db-panel__title">
                Todos los Usuarios
            </h3>
            <resource-table
                :breakpoint="800"
                :model="{{ $user }}"
                inline-template
            >
                <table
                    class="table table-resource table--header-primary md:table--responsive table--full-width table--striped"
                    :class="{ 'table-resource--wide' : wideView }"
                    >
                    <thead>
                        <tr>
                            <th>
                                Clave
                            </th>

                            <th>
                                Apellido
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Telefono
                            </th>
                            <th>
                                Perfil
                            </th>
                            <th>
                                Intentos Fallidos
                            </th>
                            <th>
                                Estatus de Cuenta
                            </th>
                            <th>
                                Correo Electronico
                            </th>
                            <th>
                                Acciones
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="userItem in resourceList" class="table-resource__row" :key="userItem.id">
                            <td data-label="Clave:">
                                @{{ userItem.key_code }}
                            </td>
                            <td data-label="Apellido:">
                                @{{ userItem.last_name }}
                            </td>
                            <td data-label="Nombre:">
                                @{{ userItem.name }}
                            </td>
                            <td data-label="Telefono:">
                                @{{ userItem.phone }}
                            </td>
                            <td data-label="Perfil:">
                                @{{ userItem.profile }}
                            </td>
                            <td data-label="Intentos Fallidos:">
                                @{{ userItem.failed_attempts }}
                            </td>
                            <td data-label="Estatus de Cuenta:">
                                @{{ userItem.account_status }}
                            </td>
                            <td data-label="Correo Electronico:">
                                @{{ userItem.email }}
                            </td>
                            
                           <td data-label="Acciones:">
                                <delete-button :url="$root.path + '/admin/users/eliminar/' + userItem.id"
                                    :resource-id="userItem.id"
                                    :options="{ onDelete: onResourceDelete }"
                                >
                                    @svg('trash')
                                    Eliminar
                                </delete-button>

                                <a class="btn" :href="$root.path + '/admin/users/editar/' + userItem.id">
                                    @svg('edit')
                                    Editar
                                </a>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </resource-table>

        </section>
    </div>

@endsection
