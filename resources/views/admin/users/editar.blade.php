@extends('layout.dashboard-master')
@section('content')
<section class="section">
    <div class="container">
        <editar-user
            action="{{ url('admin/users/actualizar/'.$user->id) }}"
            :user="{{ $user }}"
            method="PUT"
        >
        </editar-user>
    </div>
</section>
@endsection
