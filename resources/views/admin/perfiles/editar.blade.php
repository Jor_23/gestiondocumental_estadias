@extends('layout.dashboard-master')

{{-- Metadata --}}
@section('meta.title', 'Editar perfil' )
@section('meta.tab_title', 'Editar perfil | Panel de administración | ' . config('app.name'))
@section('css_classes', 'dashboard')
@section('has_gallery', 'true')

@section('content')


    <div class="dashboard-heading">
        <h1 class="dashboard-heading__title">
            Editar perfil
        </h1>
    </div>

    <div class="fluid-container mb-16">

       
                <section class="db-panel">
                    <h3 class="db-panel__title">
                        Datos del perfil
                    </h3>

                    <div class="md:row">
                        <div class="md:col-2/3">
                            <perfileditar-form 
                            action="{{ url('admin/perfiles/actualizar/'.$profile->id)}}"
                            :profile="{{ $profile }}"
                            method="PUT"
                            >
                             </perfileditar-form>

                            
                        </div>
                    </div>
                </section>


    </div>


@endsection