@extends('layout.dashboard-master')

{{-- Metadata --}}
@section('meta.title', 'Crear perfil' )
@section('meta.tab_title', 'Crear perfil | Panel de administración | ' . config('app.name'))
@section('css_classes', 'dashboard')
@section('has_gallery', 'true')

@section('content')


    <div class="dashboard-heading">
        <h1 class="dashboard-heading__title">
            Crear perfil
        </h1>
    </div>

    <div class="fluid-container mb-16">

       
                <section class="db-panel">
                    <h3 class="db-panel__title">
                        Datos del perfil
                    </h3>

                    <div class="md:row">
                        <div class="md:col-2/3">
                            
                            <perfilagregar-form 
                                action="{{ url('admin/perfiles/insertar')}}"
                            >
                             </perfilagregar-form>

                        </div>
                    </div>
                </section>


    </div>


@endsection