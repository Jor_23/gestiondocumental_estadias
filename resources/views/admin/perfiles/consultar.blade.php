@extends('layout.dashboard-master')

{{-- Metadata --}}
@section('meta.title', 'Vizualizar perfiles' )
@section('meta.tab_title', 'Vizualizar perfiles | Panel de administración | ' . config('app.name'))
@section('css_classes', 'dashboard')
@section('has_gallery', 'true')

@section('content')

<div class="dashboard-heading">
        <h1 class="dashboard-heading__title">
            Consultar perfiles
        </h1>
    </div>

    <div class="fluid-container mb-16">

        

       
        <section class="db-panel">
            <h3 class="db-panel__title">
                Perfiles encontrados
            </h3>

            

            <resource-table
                :breakpoint="800"
                :model="{{ $profile }}"
                inline-template
            >
                <table
                    class="table table-resource table--header-primary md:table--responsive table--full-width table--striped"
                    :class="{ 'table-resource--wide' : wideView }"
                    >
                    <thead>
                        <tr>
                            <th>
                                Nombre
                            </th>

                            <th>
                                Descripcion
                            </th>

                            <th>
                                Pol_sesion
                            </th>

                            <th>
                                Pol_password
                            </th>

                            <th>
                                Acción
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="profileItem in resourceList" class="table-resource__row" :key="profileItem.id">
                            <td data-label="Nombre:">
                                @{{ profileItem.name }}
                            </td>
                            <td data-label="Descripcion:">
                                @{{ profileItem.description }}
                            </td>
                            <td data-label="Pol_sesion:">
                                @{{ profileItem.pol_sesion }}
                            </td>
                            <td data-label="Pol_password:">
                                @{{ profileItem.pol_password }}
                            </td>
                            <td data-label="Acciones:">
                                <delete-button :url="$root.path + '/admin/perfiles/eliminar/' + profileItem.id"
                                    :resource-id="profileItem.id"
                                    :options="{ onDelete: onResourceDelete }"
                                >
                                    @svg('trash')
                                    Eliminar
                                </delete-button>
                                <a class="btn" :href="$root.path + '/admin/perfiles/editar/' + profileItem.id"
                                   
                                >
                                    @svg('edit')
                                    Editar
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </resource-table>

            <a class="btn" href="/admin/perfiles/agregar">
                Agregar nuevo
            </a>

        </section>


    </div>
    

</div>

@endsection