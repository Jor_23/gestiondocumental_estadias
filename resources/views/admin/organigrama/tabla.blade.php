@extends('layout.dashboard-master')

{{-- Metadata --}}
@section('meta.title', 'Visualizar Organigrama' )
@section('meta.tab_title', 'Visualizar Organigrama | Panel de administración | ' . config('app.name'))
@section('css_classes', 'dashboard')
@section('has_gallery', 'true')

@section('content')

    <div class="dashboard-heading">
        <h1 class="dashboard-heading__title">
            Organigramas
        </h1>
    </div>


    <div class="fluid-container mb-16">
        <section class="db-panel">
            <h3 class="db-panel__title">
                Todos los Organigramas
            </h3>
            <resource-table
                :breakpoint="800"
                :model="{{ $organigrama }}"
                inline-template
            >
                <table
                    class="table table-resource table--header-primary md:table--responsive table--full-width table--striped"
                    :class="{ 'table-resource--wide' : wideView }"
                    >
                    <thead>
                        <tr>
                            <th>
                                Referencia
                            </th>

                            <th>
                                Titulo
                            </th>
                            <th>
                                Descripción
                            </th>
                            <th>
                                Acciones
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="organiItem in resourceList" class="table-resource__row" :key="organiItem.id">
                            <td data-label="Referencia:">
                                @{{ organiItem.reference_key }}
                            </td>
                            <td data-label="Titulo:">
                                @{{ organiItem.title }}
                            </td>
                            <td data-label="Descripción:">
                                @{{ organiItem.description_level }}
                            </td>
                           <td data-label="Acciones:">
                                <delete-button :url="$root.path + '/admin/organigrama/eliminar/' + organiItem.id"
                                    :resource-id="organiItem.id"
                                    :options="{ onDelete: onResourceDelete }"
                                >
                                    @svg('trash')
                                    Eliminar
                                </delete-button>

                                <a class="btn" :href="$root.path + '/admin/organigrama/editar/' + organiItem.id">
                                    @svg('edit')
                                    Editar
                                </a>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </resource-table>

        </section>
    </div>

@endsection
