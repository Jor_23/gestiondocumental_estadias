@extends('layout.dashboard-master')

{{-- Metadata --}}
@section('meta.title', 'Agregar Organigrama' )
@section('meta.tab_title', 'Agregar Organigrama | Panel de administración | ' . config('app.name'))
@section('css_classes', 'dashboard')
@section('has_gallery', 'true')

@section('content')


    <div class="dashboard-heading">
        <h1 class="dashboard-heading__title">
            Agregar Organigrama
        </h1>
    </div>

    <div class="fluid-container mb-16">


                <section class="db-panel">
                    <h3 class="db-panel__title">
                        Datos del Organigrama
                    </h3>

                    <div class="md:row">
                        <div class="md:col-2/3">

                            <agregar-org action="{{ url('admin/organigrama/insertar')}}">
                             </agregar-org>

                        </div>
                    </div>
                </section>


    </div>


@endsection
