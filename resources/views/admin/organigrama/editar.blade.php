@extends('layout.dashboard-master')
@section('content')
<section class="section">
    <div class="container">
        <editar-org
            action="{{ url('admin/organigrama/actualizar/'.$organigrama->id) }}"
            :organigrama="{{ $organigrama }}"
            method="PUT"
        >
        </editar-org>
    </div>
</section>
@endsection
