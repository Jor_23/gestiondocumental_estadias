@extends('layout.master')
@section('content')
<section class="section">
    <div class="container">
        <edit-role-form
            action="{{ url('/actualizar-rol/'.$role->key_name) }}"
            :role="{{ $role }}"
            method="PUT"
        >
        </edit-role-form>
    </div>
</section>
@endsection