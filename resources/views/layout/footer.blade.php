<footer class="main-footer" role="contentinfo">

    <div class="">
        <div class="container text-center py-4">
            <img class="main-footer__logo main-footer__logo--educacion" src="{{ asset('img/footer/logo-ujed.2.png') }}" alt="Educación">
        </div>
    </div>
    <div class="sub-footer">
        <p>Constitución 404 Sur, Zona Centro. C.P. 34000, Durango, Dgo. México. Tel: (618) 827 12 00. ujed@ujed.mx</p>
    </div>
</footer>
