<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formatos extends Model
{

    protected $fillable = [
        'reference_key','title', 'description_level'
    ];


        public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }


    /**
     * Get the links that belong to the submenu.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'formatos_id');
    }

}
