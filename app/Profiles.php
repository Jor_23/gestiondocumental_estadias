<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{
   
    protected $fillable = [
        'name','description','pol_sesion','pol_password'
    ];


    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function users()
    {
        return $this->hasMany(User::class, 'profile_id');
    }

}