<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_AR extends Model
{

    protected $table = "users_ar";
    protected $fillable = [
        'key_code','last_name', 'name','phone','profile','failed_attempts','account_status','email'
    ];


        public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }


    /**
     * Get the links that belong to the submenu.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'users_ar_id');
    }

}
