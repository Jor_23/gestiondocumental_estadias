<?php

namespace App\Http\Requests;

class RoleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|string|max:60|min:5',
            'key_name' => 'required|string|max:60',
        ];
    }
}
