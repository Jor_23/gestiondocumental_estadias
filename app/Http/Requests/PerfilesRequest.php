<?php

namespace App\Http\Requests;

class PerfilesRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|string|max:30|min:5',
            'description' => 'required|string|max:40|min:5',
            'pol_sesion'     => 'required|string|max:16|min:5',
            'pol_password' => 'required|string|max:16| min:6',
        ];
    }
}
