<?php

namespace App\Http\Requests;

class OrganigramaRequest extends FormRequest
{
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
      return [
          'reference_key'     => 'required|string|max:60|min:1',
          'title' => 'required|string|max:60',
          'description_level' => 'required|string|max:60',
      ];
  }
}
