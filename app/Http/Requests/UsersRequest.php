<?php

namespace App\Http\Requests;

class UsersRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key_code'     => 'required|max:60|min:1',
            'last_name' => 'required|string|max:60',
            'name' => 'required|string|max:60',
            'phone'     => 'required|string|min:8',
            'profile' => 'required',
            'failed_attempts' => 'required',
            'account_status'     => 'required',
            'email' => 'required|max:60',

        ];
    }
}
