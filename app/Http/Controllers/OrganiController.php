<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\OrganigramaRequest;
use App\Formatos;
use Illuminate\Support\Str;

class OrganiController extends Controller
{
    public function index()
    {
        $organigrama = Formatos::all();
        return view('admin/organigrama/tabla', compact('organigrama'));
    }

    public function save(OrganigramaRequest $request)
    {
        //Guardar
        Formatos::create([
            'reference_key'     => $request->reference_key,
            'title' => $request->title,
            'description_level'     => $request->description_level,

        ]);
        return response('', 204, [
              'Redirect-To' => url('admin/organigrama/tabla')
        ]);
    }

    public function edit($id)
    {
        $organigrama = Formatos::where('id', $id)->first();
        return view('admin/organigrama/editar', compact('organigrama'));
    }

    public function update(OrganigramaRequest $request, $id)
    {
        $organigrama = Formatos::find($id);
        $organigrama->update([
            'reference_key'     => $request->reference_key,
            'title' => $request->title,
            'description_level'     => $request->description_level,
        ]);

        return response('', 204, [
              'Redirect-To' => url('admin/organigrama/tabla')
        ]);
    }

    public function delete($id)
    {
        $organigramas = Formatos::find($id)->delete();
        return response('', 204);
    }
}
