<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PerfilesRequest;
use App\Profiles;
use Illuminate\Support\Str;

class PerfilesController extends Controller
{
    public function index()
    {
        $profile = Profiles::all();
        return view('admin.perfiles.consultar', compact('profile'));
    }

    public function save(PerfilesRequest $request)
    {
        //Guardar
        Profiles::create([
            'name' => $request->name,
            'description' => $request->description,
            'pol_sesion' => $request->pol_sesion,
            'pol_password' => $request->pol_password
        ]);
        
        return response('', 204, [
            'Redirect-To' => url('admin/perfiles/consultar')
        ]);
    }

    public function edit($id)
    {
        $profile = Profiles::where('id', $id)->first();
        return view('admin.perfiles.editar', compact('profile'));
    }

    public function update(PerfilesRequest $request, $id)
    {
        $profile = Profiles::find($id);
        $profile->update([
            'name' => $request->name,
            'description' => $request->description,
            'pol_sesion' => $request->pol_sesion,
            'pol_password' => $request->pol_password

        ]);
        
        return response('', 204, [
            'Redirect-To' => url('admin/perfiles/consultar')
        ]);
    }

    public function delete($id)
    {   
        $profile = Profiles::find($id)->delete();
        return response('', 204);
    }
}
