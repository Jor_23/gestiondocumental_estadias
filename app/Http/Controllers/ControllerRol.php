<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Role;
use Illuminate\Support\Str;

class EjemploController extends Controller
{
    public function index()
    {
        $ejemplo_nombre = 'Ramón Arturo';
        return view('ejemplo', compact('ejemplo_nombre'));
    }

    public function save(RoleRequest $request)
    {
        //Guardar
        Role::create([
            'name'     => $request->name,
            'key_name' => $request->key_name
        ]);
    }

    public function edit($key_name)
    {
        $role = Role::where('key_name', $key_name)->first();
        return view('editar-rol', compact('role'));
    }

    public function update(RoleRequest $request, $key_name)
    {
        $role = Role::find($id);
        $role->update([
            'name'     => $request->name,
            'key_name' => $request->key_name
        ]);
    }

    public function delete($key_name)
    {
        $roles = Role::where('key_name', $key_name)->get();
        $role->delete();
    }
}
