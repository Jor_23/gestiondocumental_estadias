<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UsersRequest;
use App\Users_AR;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    public function index()
    {
        $user = Users_AR::all();
        return view('admin/users/tabla', compact('user'));
    }

    public function save(UsersRequest $request)
    {
        //Guardar
        Users_AR::create([
            'key_code'     => $request->key_code,
            'last_name' => $request->last_name,
            'name'     => $request->name,
            'phone'     => $request->phone,
            'profile' => $request->profile,
            'failed_attempts'     => $request->failed_attempts,
            'account_status'     => $request->account_status,
            'email' => $request->email,


        ]);

        return response('', 204, [
            'Redirect-To' => url('admin/users/tabla')
      ]);
    }

    public function edit($id)
    {
        $user = Users_AR::where('id', $id)->first();
        return view('admin/users/editar', compact('user'));
    }

    public function update(UsersRequest $request, $id)
    {
        $user = Users_AR::find($id);
        $user->update([
        'key_code'     => $request->key_code,
        'last_name' => $request->last_name,
        'name'     => $request->name,
        'phone'     => $request->phone,
        'profile' => $request->profile,
        'failed_attempts'     => $request->failed_attempts,
        'account_status'     => $request->account_status,
        'email' => $request->email,

        ]);

        return response('', 204, [
            'Redirect-To' => url('admin/users/tabla')
      ]);
    }

    public function delete($id)
    {
        $users = Users_AR::find($id)->delete();
        return response('', 204);
    }
}
